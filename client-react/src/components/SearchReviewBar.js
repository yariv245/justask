import React, { useState, useEffect } from 'react';
import Portal from '@material-ui/core/Portal';
import { makeStyles } from '@material-ui/core/styles';
import { Button, ButtonToolbar } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import '../css/FixedSideBarStyle.css';
import serverAxios from '../util/serverAxios';
import Icon from 'awesome-react-icons';

const useStyles2 = makeStyles((theme) => ({
  alert: {
    padding: theme.spacing(0),
    // margin: theme.spacing(1, 0),
  },
}));

const SearchReviewBar = ({ setList, postID, errorCallback }) => {
  const [fromDate, setFromDate] = useState(null);
  const [toDate, setToDate] = useState(null);
  var [searchUser, setSearchUser] = useState('');
  var [searchBody, setSearchBody] = useState('');

  const classes2 = useStyles2();
  const [show, setShow] = React.useState(false);
  const container = React.useRef(null);

  const handleClickFilter = () => {
    setShow(!show);
  };

  // retuen list of all tags with only name and id
  useEffect(() => {}, []);
  function ClearParams() {
    setFromDate(null);
    setToDate(null);
    setSearchUser('');
    setSearchBody('');
    serverAxios
      .get(`/reviews/getPostReviews/${postID}`)
      .then((res) => {
        const posts = res.data;
        setList(posts);
      })
      .catch((e) => {
        errorCallback(e);
      });
  }
  function FilterBy3Parameters() {
    // return new arrat with only the tags id
    serverAxios
      .get(`/reviews/search3params`, {
        params: {
          postID: postID,
          bodyReview: searchBody,
          userName: searchUser,
          fromDate: fromDate,
          toDate: toDate,
        },
      })
      .then((res) => {
        // change the list to only with the new posts
        setList(res.data);
      })
      .catch((e) => {
        errorCallback(e);
      });
  }

  return (
    <>
      <div
        style={{
          flexDirection: 'row',
          justifyItems: 'flex-start',
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyItems: 'flex-start',
          }}
        >
          <div style={{ marginLeft: '10px' }}>
            <button
              type="button"
              className="btn btn-primary"
              onClick={handleClickFilter}
            >
              {show ? (
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyItems: 'flex-start',
                    justifyContent: 'space-between',
                  }}
                >
                  <Icon name="settings" />
                  <p style={{ marginLeft: '5px', width: '100px' }}>
                    Close Filter
                  </p>
                </div>
              ) : (
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyItems: 'flex-start',
                    justifyContent: 'space-between',
                  }}
                >
                  <Icon name="settings" />
                  <p style={{ marginLeft: '5px' }}>Filter</p>
                </div>
              )}
            </button>
          </div>
          <div className={classes2.alert} ref={container} />
        </div>{' '}
        <div className={classes2.alert}>
          {show ? (
            <Portal container={container.current}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyItems: 'flex-start',
                }}
              >
                <input
                  onInput={(event) => {
                    setSearchBody(event.target.value);
                  }}
                  value={searchBody}
                  type="text"
                  className="form-control"
                  placeholder="Search body"
                  style={{ width: '300px' }}
                />{' '}
                <input
                  onInput={(event) => {
                    setSearchUser(event.target.value);
                  }}
                  value={searchUser}
                  type="text"
                  className="form-control"
                  placeholder="Enter User Name"
                  style={{ width: '300px' }}
                />{' '}
                <DatePicker
                  placeholderText="From Date"
                  className="form-control"
                  selected={fromDate}
                  onChange={(date) => {
                    setFromDate(date);
                  }}
                />
                <DatePicker
                  className="form-control"
                  selected={toDate}
                  onChange={(date) => {
                    setToDate(date);
                  }}
                  placeholderText="To Date"
                />
                <ButtonToolbar>
                  <Button
                    variant="primary"
                    onClick={FilterBy3Parameters}
                    style={{ marginLeft: '10px' }}
                  >
                    Search
                  </Button>
                </ButtonToolbar>
                <ButtonToolbar>
                  <Button
                    variant="secondary"
                    onClick={ClearParams}
                    style={{ marginLeft: '10px' }}
                  >
                    Clear
                  </Button>{' '}
                </ButtonToolbar>
              </div>
            </Portal>
          ) : null}
        </div>
      </div>
    </>
  );
};

export default SearchReviewBar;
