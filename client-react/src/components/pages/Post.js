import React, { useEffect, useState } from 'react';
import Layout from '../Layout';
import SpinnerComponent from '../SpinnerComponent';
import { timeAgo } from '../../helpers/helpersFuncs';
import 'photoswipe/dist/photoswipe.css';
import 'photoswipe/dist/default-skin/default-skin.css';
import { Gallery, Item } from 'react-photoswipe-gallery';
import TagComponent from '../TagComponent';
import Comments from '../Comments';
import serverAxios from '../../util/serverAxios';
import { Link } from 'react-router-dom';
import Error from '../Error';
import { useHistory } from 'react-router-dom';

function Post() {
  let history = useHistory();
  var postId = window.location.href;
  postId = postId.split('/')[postId.split('/').length - 1];

  var [postDetails, setPostDetails] = useState(null);
  // for error pop up
  var [errorPopup, setErrorPopup] = useState(false);
  var [errorNum, setErrorNum] = useState('500');
  useEffect(() => {
    serverAxios
      .get(`/posts/getpost`, { params: { postid: postId } })
      .then((res) => {
        const post = res.data;
        setPostDetails(post);
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  }, []);

  const errorCallback = (e) => {
    console.error(e);
    setErrorNum(e.response.status);
    setErrorPopup(true);
  };

  //Delete Post
  const removePost = () => {
    serverAxios
      .delete(`/posts/${postId}`)
      .then((res) => {
        const post = res.data;
        console.log(post);
        history.push('/');
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  };

  if (!postDetails) return <SpinnerComponent />;

  return (
    <>
      <Layout>
        <Error
          show={errorPopup}
          handleClose={() => {
            setErrorPopup(false);
          }}
          errorNum={errorNum}
        />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12" style={{ padding: '0' }}>
              <div className="card mb-4">
                <div className="card-header">
                  <div
                    className="media flex-wrap w-100 align-items-center"
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}
                  >
                    {' '}
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}
                    >
                      <img
                        src={postDetails.author.image}
                        className="d-block ui-w-40 rounded-circle"
                        alt="userImage"
                      />
                      <Link to={`/profile/${postDetails.author._id}`}>
                        <div className="media-body ml-3">
                          {' '}
                          <a
                            href="/profile/postDetails.author._id"
                            data-abc="true"
                          >
                            {postDetails.author.name}
                          </a>
                          <div className="text-muted small">
                            {' '}
                            <div>{timeAgo(postDetails.postedTime)}</div>
                          </div>
                        </div>
                      </Link>
                    </div>
                    {postDetails.author._id !=
                      localStorage.getItem('userLoggedInID') &&
                    localStorage.getItem('userAdmin') != 'true' ? (
                      <div></div>
                    ) : (
                      <div>
                        <Link to={`/editPost/${postId}`}>
                          <button style={{ marginRight: '10px' }}>
                            <img
                              src={window.location.origin + '/images/edit.png'}
                              style={{ width: '20px' }}
                            />
                          </button>
                        </Link>
                        <button onClick={removePost}>
                          {' '}
                          <img
                            src={window.location.origin + '/images/trash.png'}
                            style={{ width: '20px' }}
                          />
                        </button>
                      </div>
                    )}
                  </div>
                </div>
                <div
                  className="card"
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ marginLeft: '7px', height: 'auto' }}>
                    <b>
                      <h1 style={{ fontSize: '25px' }}>{postDetails.title}</h1>
                    </b>
                  </div>
                  <div
                    className="text-muted big"
                    style={{ marginRight: '7px' }}
                  >
                    <br></br>
                  </div>
                </div>
                <div className="card-body">
                  <p> {postDetails.description} </p>
                </div>
                <div className="card-footer">
                  <div>
                    <b>Images : </b>
                  </div>
                  <br></br>
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'flex-start',
                      justifyContent: 'space-around',
                    }}
                  >
                    {postDetails.images.length !== 0 ? (
                      postDetails.images.map((image) => {
                        return (
                          <Gallery>
                            <Item
                              original={image}
                              thumbnail={image}
                              width="1024"
                              height="768"
                            >
                              {({ ref, open }) => (
                                <img
                                  alt="img"
                                  ref={ref}
                                  onClick={open}
                                  src={image}
                                  style={{ width: '15%' }}
                                />
                              )}
                            </Item>
                          </Gallery>
                        );
                      })
                    ) : (
                      <p>* There are no images for this post *</p>
                    )}
                  </div>
                </div>
                <div className="card-footer d-flex flex-wrap justify-content-between align-items-center px-0 pt-0 pb-3">
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'flex-start',
                    }}
                  >
                    {postDetails.tags.length !== 0 ? (
                      postDetails.tags.map((tag) => {
                        return (
                          <div className="px-4 pt-3">
                            {' '}
                            <TagComponent tagDetails={tag} />
                          </div>
                        );
                      })
                    ) : (
                      <div style={{ marginLeft: '20px' }}>
                        <p>* There is no any tags for this post *</p>
                      </div>
                    )}
                  </div>
                  <div className="px-4 pt-3"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Comments
              postID={postId}
              setErrorPopup={setErrorPopup}
              setErrorNum={setErrorNum}
              errorCallback={errorCallback}
            />
          </div>
        </div>
      </Layout>
    </>
  );
}

export default Post;
