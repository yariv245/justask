import React from 'react';
import { Modal } from 'react-bootstrap';

const PopUp = ({ show, handleClose, children }) => {
  return (
    <Modal show={show} size="lg" centered onHide={handleClose}>
      {/* <Modal.Header closeButton></Modal.Header> */}
      <Modal.Body>{children}</Modal.Body>
    </Modal>
  );
};

export default PopUp;
