import React, { useEffect, useState } from "react";
import "../css/profile.css";
import "jquery";
import "popper.js";
import "bootstrap/dist/js/bootstrap.min";
import Layout from "./Layout";
import SpinnerComponent from "./SpinnerComponent";
import serverAxios from "../util/serverAxios";
import { timeAgo } from "../helpers/helpersFuncs";
import { Link } from "react-router-dom";
import Error from "./Error";
import Snackbar from "@material-ui/core/Snackbar";

function EditProfile(props) {
  // for error pop up
  var [errorPopup, setErrorPopup] = useState(false);
  var [errorNum, setErrorNum] = useState("500");
  var userIdLoggedIn = localStorage.getItem("userLoggedInID");

  var userId = window.location.href;
  userId = userId.split("/")[userId.split("/").length - 1];

  var [userProfile, setUserProfile] = useState(null);
  var [userPostsList, setUserPostsList] = useState(null);
  var [fullNameET, setFullNameET] = useState("");
  var [emailET, setEmailET] = useState("");
  /////SnackBar - On Save Succeded
  const [state, setState] = React.useState({
    open: false,
    vertical: "top",
    horizontal: "center",
  });

  const { vertical, horizontal, open } = state;

  const handleClose = () => {
    setState({ ...state, open: false });
  };

  ///////////

  useEffect(() => {
    serverAxios
      .get(`/users/${userId}`)
      .then((res) => {
        const userAuthor = res.data;
        setUserProfile(userAuthor);
        setFullNameET(userAuthor.name);
        setEmailET(userAuthor.email);
      })
      .catch((e) => {
        console.error("ERROR with getting user obj fetching data: " + e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });

    serverAxios
      .get(`/posts/ofSpecUser/${userId}`)
      .then((res) => {
        console.log("@@@@");
        console.log(res.data);
        const UserPosts = res.data;
        setUserPostsList(UserPosts);
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  }, []);

  const handleChangeEmail = (event) => {
    setEmailET(event.target.value);
  };
  const handleChangeFullName = (event) => {
    setFullNameET(event.target.value);
  };

  const handleSave = () => {
    // send post request with all the new data for specific user
    serverAxios
      .put(`/users/${userId}`, { fullName: fullNameET, email: emailET })
      .then((res) => {
        setState({
          open: true,
          ...{ vertical: "bottom", horizontal: "right" },
        });
        // this.forceUpdate();
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  };
  // verify the current user is the user who tries the edit
  if (userIdLoggedIn != userId && localStorage.getItem("userAdmin") != "true")
    return (
      <Error
        show={true}
        handleClose={() => {
          setErrorPopup(true);
        }}
        errorNum={401}
      />
    );

  if (!userProfile) {
    if (errorPopup == true) {
      return (
        <Error
          show={true}
          handleClose={() => {
            setErrorPopup(true);
          }}
          errorNum={404}
        />
      );
    }
    return <SpinnerComponent />;
  }

  return (
    <Layout>
      <Error
        show={errorPopup}
        handleClose={() => {
          setErrorPopup(false);
        }}
        errorNum={errorNum}
      />
      <div>
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={open}
          onClose={handleClose}
          message="Data saved successfully."
          key={vertical + horizontal}
        />
        <div
          className="container bootstrap snippets bootdeys"
          style={{ color: "inherit" }}
        >
          {userProfile != null ? (
            <div className="row" id="user-profile">
              <div className="col-lg-3 col-md-4 col-sm-4">
                <div className="main-box clearfix">
                  <h2>{userProfile.name} </h2>
                  <div className="profile-status">
                    <i className="fa fa-check-circle" /> Online
                  </div>
                  <img
                    src={userProfile.image}
                    alt="profileImage"
                    className="profile-img img-responsive center-block"
                  />
                  <div className="profile-label">
                    <b>
                      <span className="label label-danger">
                        {userProfile.isAdmin ? "Admin" : "User"}
                      </span>
                    </b>
                  </div>

                  <div className="profile-since">
                    Member since: {userProfile.singUpTime}
                  </div>
                  <div className="profile-details">
                    <ul className="fa-ul">
                      <li>
                        <i className="fa-li fa fa-comment" />
                        {userPostsList != null ? (
                          <div>
                            Posts: <span>12</span>
                          </div>
                        ) : (
                          <div>
                            Posts: <span>0</span>
                          </div>
                        )}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-9 col-md-8 col-sm-8">
                <div className="main-box clearfix">
                  <div className="profile-header">
                    <h3>
                      <span>User info</span>
                    </h3>
                    <button
                      className="btn btn-primary edit-profile"
                      onClick={handleSave}
                    >
                      <i className="fa fa-pencil-square fa-lg" /> Save
                    </button>
                  </div>
                  <br></br>
                  <div className="row profile-user-info">
                    <div className="col-sm-8">
                      <div className="profile-user-details clearfix">
                        <div className="profile-user-details-label">
                          Full Name
                        </div>
                        <div className="profile-user-details-value">
                          <input
                            type="text"
                            style={{
                              height: "auto",
                              width: "auto",
                              borderWidth: "1px",
                            }}
                            value={fullNameET}
                            onChange={handleChangeFullName}
                          />
                        </div>
                      </div>

                      <div className="profile-user-details clearfix">
                        <div className="profile-user-details-label">Email</div>
                        <div className="profile-user-details-value">
                          <input
                            type="text"
                            style={{
                              height: "auto",
                              width: "auto",
                              borderWidth: "1px",
                            }}
                            value={emailET}
                            onChange={handleChangeEmail}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-4 profile-social"></div>
                  </div>
                  <div className="tabs-wrapper profile-tabs">
                    <ul className="nav nav-tabs">
                      <li className="active">
                        <a href="#tab-activity" data-toggle="tab">
                          Posts Related
                        </a>
                      </li>
                    </ul>
                    <div className="tab-content">
                      <div
                        className="tab-pane fade in active"
                        id="tab-activity"
                      >
                        <div className="table-responsive">
                          <table className="table">
                            <tbody>
                              {userPostsList != null ? (
                                userPostsList.posts != null ? (
                                  <div>
                                    {userPostsList.posts.map((postDetails) => (
                                      <tr>
                                        <td className="text-center">
                                          <i className="fa fa-comment" />
                                        </td>
                                        <Link to={`/posts/${postDetails._id}`}>
                                          <td>{postDetails.title}</td>
                                        </Link>
                                        <td>
                                          {timeAgo(postDetails.postedTime)}
                                        </td>
                                      </tr>
                                    ))}
                                  </div>
                                ) : null
                              ) : null}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    </Layout>
  );
}

export default EditProfile;
