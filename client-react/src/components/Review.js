import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { timeAgo } from '../helpers/helpersFuncs';
import { useHistory } from 'react-router-dom';
import serverAxios from '../util/serverAxios';
import Error from './Error';
import EditReview from './EditReview';

const Review = (props) => {
  let history = useHistory();
  var authorOfReview = props.userObjectAuthor;
  // for error pop up
  var [errorPopup, setErrorPopup] = useState(false);
  var [errorNum, setErrorNum] = useState('500');
  var [showEditReview, setShowEditReview] = useState(false);

  //Delete Review
  const removeReview = () => {
    serverAxios
      .delete(`/reviews/${props.reviewID}`)
      .then((res) => {
        props.deleteCallbacks(props.reviewID);
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  };

  return (
    <>
      <Error
        show={errorPopup}
        handleClose={() => {
          setErrorPopup(false);
        }}
        errorNum={errorNum}
      />
      {showEditReview ? (
        <EditReview
          userObjectAuthor={props.userObjectAuthor}
          reviewBody={props.reviewBody}
          reviewID={props.reviewID}
          postID={props.postID}
          changeShowOfEditReview={setShowEditReview}
          updateCallbacks={props.updateCallbacks}
        />
      ) : (
        <>
          <div
            className="card-body py-3"
            style={{
              paddingTop: '20px',
            }}
          >
            <div className="row no-gutters align-items-center">
              <div className="col">{props.content}</div>
              <div className="d-none d-md-block col-4">
                <div className="row no-gutters align-items-center"></div>
              </div>
              <div className="d-none d-md-block col-1">
                {authorOfReview._id != localStorage.getItem('userLoggedInID') &&
                localStorage.getItem('userAdmin') != 'true' ? (
                  <div></div>
                ) : (
                  <div className="media-body flex-truncate ml-2">
                    <button
                      style={{ marginRight: '10px' }}
                      onClick={() => setShowEditReview(true)}
                    >
                      <img
                        src={window.location.origin + '/images/edit.png'}
                        style={{ width: '20px' }}
                      />
                    </button>

                    <button onClick={removeReview}>
                      {' '}
                      <img
                        src={window.location.origin + '/images/trash.png'}
                        style={{ width: '20px' }}
                      />
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>

          <div className="card-footer d-flex flex-wrap justify-content-between align-items-center px-0 pt-0 pb-3">
            <Link to={`/profile/${props.userObjectAuthor._id}`}>
              <div className="media col-12 align-items-center">
                {' '}
                <img
                  style={{
                    marginTop: '10px',
                  }}
                  src={
                    authorOfReview != null
                      ? authorOfReview.image != null
                        ? authorOfReview.image
                        : 'https://lh3.googleusercontent.com/proxy/O8t5v_QWbPEEhebJCW2wReuPSb7-dOIo4-2gR6--Sa4YuzOvQdKzmfnbn5r48jJHhxWVJfPm6rPdoOLUjJV8YPrXwxa9xB8OgN-plko7AySBRt_2PbpkBkk'
                      : null
                  }
                  className="d-block ui-w-30 rounded-circle"
                />
                {authorOfReview != null ? (
                  <div
                    className="media-body flex-truncate ml-2"
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}
                  >
                    <span
                      data-abc="true"
                      style={{
                        paddingTop: '10px',
                      }}
                    >
                      <b>{authorOfReview.name}</b>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          alignItems: 'flex-start',
                          paddingTop: '0px',
                        }}
                      >
                        <div
                          className=""
                          style={{
                            marginRight: '10px',
                          }}
                        >
                          <span>Member Since: </span>
                        </div>
                        {authorOfReview != null ? (
                          <div className="">{authorOfReview.singUpTime}</div>
                        ) : (
                          <div className=""></div>
                        )}
                      </div>
                    </span>
                  </div>
                ) : null}
              </div>
            </Link>

            <div className="px-4 pt-3">
              <div className="line-height-1 text-truncate">
                {timeAgo(props.date)}
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default Review;
