import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { timeAgo } from "../helpers/helpersFuncs";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import serverAxios from "../util/serverAxios";
import Error from "./Error";
import { useHistory } from "react-router-dom";

const EditReview = (props) => {
  let history = useHistory();
  var authorOfReview = props.userObjectAuthor;

  var [bodyET, setBodyET] = useState("");
  // for error pop up
  var [errorPopup, setErrorPopup] = useState(false);
  var [errorNum, setErrorNum] = useState("500");

  const handleChangeBody = (event) => {
    setBodyET(event.target.value);
  };

  useEffect(() => {
    setBodyET(props.reviewBody);
  }, []);

  const handleSaveButton = () => {
    // send post request with all the new data for specific Review
    serverAxios
      .put(`/reviews/${props.reviewID}`, {
        body: bodyET,
      })
      .then((res) => {
        props.changeShowOfEditReview(false);
        props.updateCallbacks(props.reviewID, bodyET);
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  };

  return (
    <>
      <Error
        show={errorPopup}
        handleClose={() => {
          setErrorPopup(false);
        }}
        errorNum={errorNum}
      />
      <div className="card-body py-3">
        <div className="row no-gutters align-items-center">
          <div className="col">
            <TextareaAutosize
              style={{
                fontSize: "25px",
                height: "auto",
                width: "100%",
                borderWidth: "1px",
              }}
              aria-label="empty textarea"
              value={bodyET}
              onChange={handleChangeBody}
            />
          </div>
          <div className="d-none d-md-block col-4">
            <div className="row no-gutters align-items-center">
              <Link to={`/profile/${props.userObjectAuthor}`}>
                <div className="media col-12 align-items-center">
                  {" "}
                  <img
                    src={
                      authorOfReview != null
                        ? authorOfReview.image != null
                          ? authorOfReview.image
                          : "https://lh3.googleusercontent.com/proxy/O8t5v_QWbPEEhebJCW2wReuPSb7-dOIo4-2gR6--Sa4YuzOvQdKzmfnbn5r48jJHhxWVJfPm6rPdoOLUjJV8YPrXwxa9xB8OgN-plko7AySBRt_2PbpkBkk"
                        : null
                    }
                    className="d-block ui-w-30 rounded-circle"
                  />
                  {authorOfReview != null ? (
                    <div className="media-body flex-truncate ml-2">
                      <div className="line-height-1 text-truncate">
                        {timeAgo(props.date)}
                      </div>{" "}
                      <p
                        className="text-muted small text-truncate"
                        data-abc="true"
                      >
                        by {authorOfReview.name}
                      </p>
                    </div>
                  ) : null}
                </div>
              </Link>
            </div>
          </div>
          <div className="d-none d-md-block col-1">
            <button
              type="button"
              className="btn btn-primary"
              onClick={handleSaveButton}
            >
              Save
            </button>
          </div>
        </div>
      </div>

      <div className="card-footer d-flex flex-wrap justify-content-between align-items-center px-0 pt-0 pb-3">
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "flex-start",
          }}
        >
          <div className="px-3 pt-3"> </div>
          <div className="px-0 pt-3">
            <b>Member Since: </b>
          </div>
          {authorOfReview != null ? (
            <div className="px-0 pt-3">{authorOfReview.singUpTime}</div>
          ) : (
            <div className="px-0 pt-3"></div>
          )}
        </div>
      </div>
    </>
  );
};

export default EditReview;
