import React, { useEffect, useState } from "react";
import Layout from "./Layout";
import SpinnerComponent from "./SpinnerComponent";
import { timeAgo } from "../helpers/helpersFuncs";
import "photoswipe/dist/photoswipe.css";
import "photoswipe/dist/default-skin/default-skin.css";
import { Gallery, Item } from "react-photoswipe-gallery";
import TagComponent from "./TagComponent";
import Comments from "./Comments";
import serverAxios from "../util/serverAxios";
import { Link } from "react-router-dom";
import Error from "./Error";
import Snackbar from "@material-ui/core/Snackbar";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import { useHistory } from "react-router-dom";

function EditPost() {
  // Hook for redirect page
  let history = useHistory();
  var postId = window.location.href;
  postId = postId.split("/")[postId.split("/").length - 1];

  var [postDetails, setPostDetails] = useState(null);
  // for error pop up
  var [errorPopup, setErrorPopup] = useState(false);
  var [errorNum, setErrorNum] = useState("500");
  var userIdLoggedIn = localStorage.getItem("userLoggedInID");

  var [titleET, setTitleET] = useState("");
  var [descriptionET, setDescriptionET] = useState("");

  /////SnackBar - On Save Succeded
  const [state, setState] = React.useState({
    open: false,
    vertical: "top",
    horizontal: "center",
  });

  const { vertical, horizontal, open } = state;

  const handleClose = () => {
    setState({ ...state, open: false });
  };

  ///////////

  useEffect(() => {
    serverAxios
      .get(`/posts/getpost`, { params: { postid: postId } })
      .then((res) => {
        const post = res.data;
        setPostDetails(post);
        setTitleET(post.title);
        setDescriptionET(post.description);
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  }, []);

  const handleChangeTitle = (event) => {
    setTitleET(event.target.value);
  };
  const handleChangeDescription = (event) => {
    setDescriptionET(event.target.value);
  };

  const handleSave = () => {
    // send post request with all the new data for specific post
    serverAxios
      .put(`/posts/${postId}`, { title: titleET, description: descriptionET })
      .then((res) => {
        console.log(res.data);
        setState({
          open: true,
          ...{ vertical: "bottom", horizontal: "right" },
        });
        history.push(`/posts/${postId}`);
        // this.forceUpdate();
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  };

  if (!postDetails) {
    if (errorPopup == true) {
      return (
        <Error
          show={true}
          handleClose={() => {
            setErrorPopup(true);
          }}
          errorNum={404}
        />
      );
    }
    return <SpinnerComponent />;
  }

  // verify the current user is the user who who created the post
  if (
    userIdLoggedIn != postDetails.author._id &&
    localStorage.getItem("userAdmin") != "true"
  )
    return (
      <Error
        show={true}
        handleClose={() => {
          setErrorPopup(true);
        }}
        errorNum={401}
      />
    );
  return (
    <>
      <Layout>
        <Error
          show={errorPopup}
          handleClose={() => {
            setErrorPopup(false);
          }}
          errorNum={errorNum}
        />
        <div className="container-fluid">
          <Snackbar
            anchorOrigin={{ vertical, horizontal }}
            open={open}
            onClose={handleClose}
            message="Data saved successfully."
            key={vertical + horizontal}
          />
          <div className="row">
            <div className="col-md-12">
              <div className="card mb-4">
                <div className="card-header">
                  <div
                    className="media flex-wrap w-100 align-items-center"
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    {" "}
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <img
                        src={postDetails.author.image}
                        className="d-block ui-w-40 rounded-circle"
                        alt="userImage"
                      />
                      <Link to={`/profile/${postDetails.author._id}`}>
                        <div className="media-body ml-3">
                          {" "}
                          <a
                            href="/profile/postDetails.author._id"
                            data-abc="true"
                          >
                            {postDetails.author.name}
                          </a>
                          <div className="text-muted small">
                            {" "}
                            <div>{timeAgo(postDetails.postedTime)}</div>
                          </div>
                        </div>
                      </Link>
                    </div>
                    <div>
                      <button
                        style={{ marginRight: "10px" }}
                        className="btn btn-primary edit-profile"
                        onClick={handleSave}
                      >
                        Save Changes
                      </button>
                    </div>
                  </div>
                </div>
                <div
                  className="card"
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                    justifyContent: "space-between",
                  }}
                >
                  <div
                    style={{ marginLeft: "7px", height: "auto", width: "100%" }}
                  >
                    <TextareaAutosize
                      style={{
                        fontSize: "25px",
                        height: "auto",
                        width: "100%",
                        borderWidth: "1px",
                      }}
                      aria-label="empty textarea"
                      value={titleET}
                      onChange={handleChangeTitle}
                    />
                  </div>
                  <div
                    className="text-muted big"
                    style={{ marginRight: "7px" }}
                  >
                    <br></br>
                  </div>
                </div>
                <div className="card-body">
                  <TextareaAutosize
                    style={{
                      fontSize: "18px",
                      height: "auto",
                      width: "100%",
                      borderWidth: "1px",
                    }}
                    aria-label="empty textarea"
                    value={descriptionET}
                    onChange={handleChangeDescription}
                  />
                </div>
                <div className="card-footer">
                  <div>
                    <b>Images : </b>
                  </div>
                  <br></br>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-start",
                      justifyContent: "space-around",
                    }}
                  >
                    {postDetails.images.length !== 0 ? (
                      postDetails.images.map((image) => {
                        return (
                          <Gallery>
                            <Item
                              original={image}
                              thumbnail={image}
                              width="1024"
                              height="768"
                            >
                              {({ ref, open }) => (
                                <img
                                  alt="img"
                                  ref={ref}
                                  onClick={open}
                                  src={image}
                                  style={{ width: "15%" }}
                                />
                              )}
                            </Item>
                          </Gallery>
                        );
                      })
                    ) : (
                      <p>* There are no images for this post *</p>
                    )}
                  </div>
                </div>
                <div className="card-footer d-flex flex-wrap justify-content-between align-items-center px-0 pt-0 pb-3">
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-start",
                    }}
                  >
                    {postDetails.tags.length !== 0 ? (
                      postDetails.tags.map((tag) => {
                        return (
                          <div className="px-4 pt-3">
                            {" "}
                            <TagComponent tagDetails={tag} />
                          </div>
                        );
                      })
                    ) : (
                      <div style={{ marginLeft: "20px" }}>
                        <p>* There is no any tags for this post *</p>
                      </div>
                    )}
                  </div>
                  <div className="px-4 pt-3"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <Comments
            postID={postId}
            setErrorPopup={setErrorPopup}
            setErrorNum={setErrorNum}
          />
        </div>
      </Layout>
    </>
  );
}

export default EditPost;
