import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './css/forum.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Post from './components/pages/Post';
import List from './components/pages/List';
import NewPost from './components/pages/NewPost';
import Profile from './components/pages/Profile';
import EditUser from './components/pages/editUser';
import './css/main.bundle.css';
import './css/Login.css';
import EditProfile from './components/EditProfile';
import EditPost from './components/EditPost';
import io from 'socket.io-client'; 
const socket = io.connect("http://localhost:8080")

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={List} />
      <Route path="/posts/:id" component={Post} />
      <Route path="/newpost" component={NewPost} />
      <Route path="/Profile/:id" component={Profile} />
      <Route path="/tags/:id" component={List} />
      <Route path="/EditProfile/:id" component={EditProfile} />
      <Route path="/editPost/:id" component={EditPost} />
      <Route
        path="/edituser"
        component={EditUser}
        render={() => <EditUser email={`Testin@test.co.il`} name={`testing`} />}
      />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);
