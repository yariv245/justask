const express = require('express');
const router = express.Router();
const { Tag } = require('../models/tag');
const mongoose = require('mongoose');
const checkIfTagExist = require('../helper/checkIfTagExist');
const scrape = require('../helper/scraper');
const verifyToken = require('../helper/jwt');

// //Get all tags
router.get('/', async (req, res) => {
  const tagList = await Tag.find()
    .then((tags) => res.send(tags))
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

// //Get all tags only names and Ids
router.get('/nameAndId', async (req, res) => {
  Tag.find()
    .select('type')
    .then((tags) => res.send(tags))
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

//Get tag by id
router.get('/gettag', async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: 'Exeption occured.\nPlease try again later',
    });
  }
  const tag = await Tag.findOne({
    _id: mongoose.Types.ObjectId(req.params.id),
  })
    .then((tags) => {
      if (!tags) return res.status(404).send("Tag not found");
      res.send(tags);
    })
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

// //Get tag by name
router.get('/getbyname', async (req, res) => {
  if (!req.body.type) return res.status(400).send('Missing tag name');
  Tag.findOne({ type: req.body.type })
    .then((tag) => res.send(tag))
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

// //Create tag
router.post('/addTag', verifyToken, async (req, res) => {
  if (!req.body.type) {
    return res.status(400).send('The Tag can not be created - missing type');
  }

  //Check if the tag exist in db
  let existTag = await checkIfTagExist(req.body.type);

  //If exist then return
  if (existTag !== null) return res.send(existTag.toObject());

  //Create new Tag
  var tag = new Tag({
    type: req.body.type,
  });

  //Save the tag to db
  tag
    .save()
    .then((tag) => {
      res.status(200).send(tag);
    })
    .catch((err) => {
      res.status(400).send(err);
    });
});

// //Update Tag
router.put('/:id', verifyToken, async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).send('Tag id invalid');
  }
  const tag = await Tag.findByIdAndUpdate(
    mongoose.Types.ObjectId(req.params.id),
    {
      type: req.body.type,
    },
    { new: true }
  )
    .then((tag) => res.send(tag))
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

// //Delete tag
router.delete('/:id', verifyToken, async (req, res) => {
  if (!mongoose.isValidObjectId(req.body.id)) {
    return res.status(400).json({
      success: false,
      message: 'Exeption occured.\nPlease try again later',
    });
  }
  Tag.deleteOne({
    _id: mongoose.Types.ObjectId(req.params.id),
  })
    .then(function () {
      return res
        .status(200)
        .json({ success: true, message: "The Tag has been deleted" });
    })
    .catch(function (error) {
      return res
      .status(404)
      .json({ success: false, message: "Cannot find the Tag" });
    });
});

// //Get the count of the tags
router.get('/get/count', async (req, res) => {
  Tag.countDocuments((count) => count)
    .then((countTags) => res.send({ countTags: countTags }))
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

// Get the psots of the tag
router.get('/getposts', async (req, res) => {
  //verify tag id send from client
  if (!req.query.tagId)
    return res.status(500).send('missing required id parameter');

  const objID = mongoose.Types.ObjectId(req.query.tagId);
  Tag.findOne({ _id: objID })
    .populate('posts')
    .then((tag) => res.send(tag.posts))
    .catch((err) => res.status(500).send('Faild to get the posts:' + err));
});

//Get the top 5 tags
router.get('/top5tags', async (req, res) => {
  // create new obj: id,type,posts_length -> sort it by posts_length -> limit to only 5 objs --> return the top 5 tags
  Tag.aggregate([
    { $project: { type: 1, posts_length: { $size: '$posts' } } },
    { $sort: { posts_length: -1 } },
  ])
    .limit(5)
    .then((tags) => res.status(200).send(tags))
    .catch((err) => {
      res.status(500).send('Error: ' + err);
    });
});

//scrape stackoverflow tag page
router.get('/scraper', verifyToken, async (req, res) => {
  var pages = 3;
  if (req.params.pages) pages = req.params.pages;
  scrape(pages)
    .then(() => res.send('Pages Scraped !'))
    .catch((err) => res.send('Error: ' + err));
});

module.exports = router;
