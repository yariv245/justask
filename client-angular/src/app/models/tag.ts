import {Post} from './post';

export interface Tag{
    _id:string,
    type:string,
    posts:[Post],
}