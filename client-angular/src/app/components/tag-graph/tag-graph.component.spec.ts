import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TagGraphComponent } from './tag-graph.component';

describe('TagGraphComponent', () => {
  let component: TagGraphComponent;
  let fixture: ComponentFixture<TagGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TagGraphComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TagGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
