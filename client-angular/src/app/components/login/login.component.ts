import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  private email: string;
  private password: string;
  onError: boolean = false;
  constructor(private postsService: PostsService) {}
  ngOnInit(): void {}
  EmailonKey(event: any) {
    // without type info
    this.email = event.target.value;
  }
  PasswordonKey(event: any) {
    // without type info
    this.password = event.target.value;
  }
  @Input() public isLoggedIn: () => void;

  close() {
    this.onError = false;
  }
  onLoginlick() {
    this.postsService.login(this.email, this.password).subscribe(
      (data) => {
        if (data.user.isAdmin === true) {
          sessionStorage.setItem('token', data.token);
          this.isLoggedIn();
          window.location.reload();
        } else {
          this.onError = true;
          console.error('You are not an Admin !');
        }
      },
      (err) => {
        this.onError = true;
        console.error(err);
      }
    );
  }
}
