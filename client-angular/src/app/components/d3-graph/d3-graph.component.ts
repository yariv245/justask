import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-d3-graph',
  templateUrl: './d3-graph.component.html',
  styleUrls: ['./d3-graph.component.css'],
})
export class D3GraphComponent implements OnInit {
  constructor() {}
  hideTagsGraph: boolean = true;
  hideUsersGraph: boolean = false;

  ngOnInit(): void {}

  onTagClick() {
    this.hideTagsGraph = !this.hideTagsGraph;
    this.hideUsersGraph = false;
  }
  onUserClick() {
    this.hideUsersGraph = !this.hideUsersGraph;
    this.hideTagsGraph = false;
  }
}
