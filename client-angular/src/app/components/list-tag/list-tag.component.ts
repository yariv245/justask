import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewContainerRef,
} from '@angular/core';
import { AppComponent } from '../../app.component';
import { PostsService } from '../../services/posts.service';
import { Tag } from '../../models/tag';


@Component({
  selector: 'app-list-tag',
  templateUrl: './list-tag.component.html',
  styleUrls: ['./list-tag.component.css']
})
export class ListTagComponent implements OnInit {

  @Input()
  tag: Tag;
  _parent: AppComponent;
  @Input() index: string;

  constructor(
    private postsService: PostsService,
    private elRef: ElementRef,
    private viewContainerRef: ViewContainerRef,
  ) {
    const _injector = this.viewContainerRef.parentInjector;
    this._parent = _injector.get<AppComponent>(AppComponent);
  }

  ngOnInit(): void {}

  onDeletePost(): void {
    this.postsService.deleteTag(this.tag._id).subscribe((deleted) => {
      // console.log(this._parent.myPosts.length);
      // delete this._parent.myPosts[+this.index];
      // console.log('deleted');
      // console.log(this._parent.myPosts.length);
      this.elRef.nativeElement.parentElement.style.display = 'none';
    });
  }
}
